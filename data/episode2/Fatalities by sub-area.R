###################################################################################
##                          Dataset: Monthly Fatalities                          ##
## Source 1: NHTSA Early Estimate of Motor Vehicle Traffic Fatalities, June 2021 ##
##          Source 2: FIRST query for 2019, Persons Killed in Fatal Crashes      ##
##                             Author: Charles Noble                             ##
##                                 Date: 20211115                                ##
###################################################################################

## import
subarea_fatalities = read.csv("Economic recovery chapter/Data/Early Estimates of Motor Vehicle Traffic Fatalities and Fatality Rate by Sub-Categories in 2020.csv")

## create columns that can be graphed
subarea_fatalities$Total.Fatalities = gsub(",","",subarea_fatalities$Total.Fatalities)
subarea_fatalities$Total.Fatalities = as.numeric(as.character(subarea_fatalities$Total.Fatalities))

subarea_fatalities$Month = match(subarea_fatalities$Month, month.name)

library(zoo)
subarea_fatalities$Date = as.yearmon(paste(subarea_fatalities$ï..Year, subarea_fatalities$Month), "%Y %m")
subarea_fatalities$Date = as.Date(subarea_fatalities$Date)
subarea_fatalities = subarea_fatalities[-c(1:2, 34:44)]
subarea_fatalities = subarea_fatalities[ , c("Date",
                                              names(subarea_fatalities)[names(subarea_fatalities) != "Date"])]

subarea_fatalities = as.data.frame(lapply(subarea_fatalities, function(y) gsub("%", "", y)))
subarea_fatalities[,2:ncol(subarea_fatalities)] = lapply(subarea_fatalities[,2:ncol(subarea_fatalities)], function(y) as.numeric(as.character(y)))
View(subarea_fatalities)

subarea_fatalities$Date = as.Date(subarea_fatalities$Date)

names(subarea_fatalities)[9] = "Less_than_16"
names(subarea_fatalities)[10] = "Between_16_and_24"
names(subarea_fatalities)[11] = "Between_25_and_34"
names(subarea_fatalities)[12] = "Between_35_and_44"
names(subarea_fatalities)[13] = "Between_45_and_54"
names(subarea_fatalities)[14] = "Between_55_and_64"
names(subarea_fatalities)[15] = "Above_65"

subarea_fatalities = subarea_fatalities[complete.cases(subarea_fatalities), ]

## combine age groups so graph only has four groups
subarea_fatalities$Between_16_and_34 = subarea_fatalities[10] + subarea_fatalities[11]
subarea_fatalities$Between_35_and_54 = subarea_fatalities[12] + subarea_fatalities[13]
subarea_fatalities$Above_55 = subarea_fatalities[14] + subarea_fatalities[15]

## now save the final dataframe
write.csv(subarea_fatalities,'Economic recovery chapter/Data/Saved cleaned data/subarea_fatalities.csv')


## cleaning complete - now for plotting
library(ggplot2)
library(dplyr)
library(scales)

## by area
monthly_system_graph =
  ggplot(subarea_fatalities, aes(x = Date, group = 6)) + 
  geom_line(aes(y = Rural.Interstate, color = "Rural Interstate"), size = 1) +
  geom_line(aes(y = Urban.Interstate, color = "Urban Interstate"), size = 1) +
  geom_line(aes(y = Rural.Arterial, color = "Rural Arterial"), size = 1) +
  geom_line(aes(y = Urban.Arterial, color = "Urban Arterial"), size = 1) +
  geom_line(aes(y = Rural.Collector..Local, color = "Rural Collector/Local"), size = 1) +
  geom_line(aes(y = Urban.Collector.Local, color = "Urban Collector/Local"), size = 1) +
  #ggtitle("Percentage of Monthly Fatalities by Road Type") + 
  theme(plot.title = element_text(hjust = 0.5)) +
  xlab("Month") + 
  ylab("Percent of Total Fatalities") +
  scale_colour_manual("", 
                      breaks = c("Rural Interstate", "Urban Interstate", "Rural Arterial", "Urban Arterial", "Rural Collector/Local", "Urban Collector/Local"),
                      values = c("red", "steelblue", "forestgreen", "orange", "purple", "brown")) 

print(monthly_system_graph)

## now save the graph
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Road Type.pdf", plot = monthly_system_graph)
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Road Type.jpg", plot = monthly_system_graph)


## by age
monthly_age_graph =
  ggplot(subarea_fatalities, aes(x = Date, group = 7)) + 
  #geom_line(aes(y = Less_than_16, color = "Less than 16"), size = 1) +
  geom_line(aes(y = Between_16_and_24, color = "Between 16-34"), size = 1) +
  geom_line(aes(y = Between_35_and_44, color = "Between 35-54"), size = 1) +
  geom_line(aes(y = Above_65, color = "Above 55"), size = 1) +
  #ggtitle("Percentage of Monthly Fatalities by Age Group") + 
  theme(plot.title = element_text(hjust = 0.5)) +
  xlab("Month") + 
  ylab("Percent of Total Fatalities") +
  scale_colour_manual("", 
                      breaks = c("Between 16-34", "Between 35-54", "Above 55"),
                      values = c("red", "steelblue", "darkgreen")) 

print(monthly_age_graph)

## now save the graph
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Age Group.pdf", plot = monthly_age_graph)
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Age Group.png", plot = monthly_age_graph)


## by gender
monthly_gender_graph =
  ggplot(subarea_fatalities, aes(x = Date, group = 2)) + 
  geom_line(aes(y = Male, color = "Male"), size = 1) +
  geom_line(aes(y = Female, color = "Female"), size = 1) +
  #ggtitle("Percentage of Monthly Fatalities by Gender") + 
  theme(plot.title = element_text(hjust = 0.5)) +
  xlab("Month") + 
  ylab("Percent of Total Fatalities") +
  scale_colour_manual("", 
                      breaks = c("Male", "Female"),
                      values = c("red", "steelblue")) 

print(monthly_gender_graph)

## also do pie chart, since the data are fairly even
library(waffle) ## waffle instead!
library(ggthemes)

parts = c("Male" = round(mean(subarea_fatalities$Male), digits = 0), "Female" = round(mean(subarea_fatalities$Female), digits = 0))
names(parts) = paste0(names(parts),"(",parts,"/",sum(parts),")")
gender_waffle = waffle(parts, colors = c("darkred", "lightblue"))

## now save the graph and pie chart
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Gender.pdf", plot = monthly_gender_graph)
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Gender.jpg", plot = monthly_gender_graph)

ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Gender Waffle.pdf", plot = gender_waffle)
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Gender Waffle.jpg", plot = gender_waffle)

## by number of vehicles
monthly_number_graph =
  ggplot(subarea_fatalities, aes(x = Date, group = 2)) + 
  geom_line(aes(y = Single.Vehicle, color = "Single Vehicle"), size = 1) +
  geom_line(aes(y = Multi.Vehicle, color = "Multi-Vehicle"), size = 1) +
  #ggtitle("Percentage of Monthly Fatalities by Number of Vehicles") + 
  theme(plot.title = element_text(hjust = 0.5)) +
  xlab("Month") + 
  ylab("Percent of Total Fatalities") +
  scale_colour_manual("", 
                      breaks = c("Single Vehicle", "Multi-Vehicle"),
                      values = c("red", "steelblue")) 

print(monthly_number_graph)

## now save the graph
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Number of Vehicles.pdf", plot = monthly_number_graph)
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Number of Vehicles.jpg", plot = monthly_number_graph)

## by site
monthly_site_graph =
  ggplot(subarea_fatalities, aes(x = Date, group = 2)) + 
  geom_line(aes(y = Departure, color = "Departure"), size = 1) +
  geom_line(aes(y = On.Road, color = "On Road"), size = 1) +
  #ggtitle("Percentage of Monthly Fatalities by Site") + 
  theme(plot.title = element_text(hjust = 0.5)) +
  xlab("Month") + 
  ylab("Percent of Total Fatalities") +
  scale_colour_manual("", 
                      breaks = c("Departure", "On Road"),
                      values = c("red", "steelblue")) 

print(monthly_site_graph)

## now save the graph
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Site.pdf", plot = monthly_site_graph)
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Site.jpg", plot = monthly_site_graph)

## by roadway user
monthly_user_graph =
  ggplot(subarea_fatalities, aes(x = Date, group = 4)) + 
  geom_line(aes(y = Motorcyclists, color = "Motorcyclists"), size = 1) +
  geom_line(aes(y = Pedestrians, color = "Pedestrians"), size = 1) +
  geom_line(aes(y = Pedalcyclists, color = "Pedalcyclists"), size = 1) +
  #geom_line(aes(y = Involving.Large.Trucks, color = "Involving Large Trucks"), size = 1) +
  #ggtitle("Percentage of Monthly Fatalities by Road User") + 
  theme(plot.title = element_text(hjust = 0.5)) +
  xlab("Month") + 
  ylab("Percent of Total Fatalities") +
  scale_colour_manual("", 
                      breaks = c("Motorcyclists", "Pedestrians", "Pedalcyclists"),
                      values = c("red", "steelblue", "green"))

print(monthly_user_graph)

## now save the graph
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Roadway User.pdf", plot = monthly_user_graph)
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Roadway User.jpg", plot = monthly_user_graph)


## by race
monthly_race_graph =
  ggplot(subarea_fatalities, aes(x = Date, group = 5)) + 
  geom_line(aes(y = White, color = "White"), size = 1) +
  geom_line(aes(y = Black, color = "Black"), size = 1) +
  geom_line(aes(y = American.Indian, color = "American Indian"), size = 1) +
  geom_line(aes(y = Asian.Pacific.Islander, color = "Asian/Pacific Islander"), size = 1) +
  geom_line(aes(y = All.Other.Races, color = "All Other"), size = 1) +
  #ggtitle("Percentage of Monthly Fatalities by Race") + 
  theme(plot.title = element_text(hjust = 0.5)) +
  xlab("Month") + 
  ylab("Percent of Total Fatalities") +
  scale_colour_manual("", 
                      breaks = c("White", "Black", "American Indian", "Asian/Pacific Islander", "All Other Races"),
                      values = c("red", "steelblue", "green", "orange", "purple")) 

print(monthly_race_graph)

## now save the graph
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Race.pdf", plot = monthly_race_graph)
ggsave("Economic recovery chapter/Graphs/Percentage of Monthly Fatalities by Race.jpg", plot = monthly_race_graph)


## for the fatalities infobox, sum 2020 total fatalities by type
data_2020 = subset(subarea_fatalities, grepl("2020", Date, ignore.case = TRUE))
data_2019 = subset(subarea_fatalities, grepl("2019", Date, ignore.case = TRUE))

data_2020 = data.frame(colSums(data_2020[,-1]))




write.csv(subarea_fatalities,'Economic recovery chapter/Data/Saved cleaned data/subarea_fatalities.csv')
