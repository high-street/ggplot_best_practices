#################################### INFO ######################################
#
# AUTHOR: Rob Owens
# DATE: 2/14/2022
# PURPOSE: EPISODE THREE HOW TO CUSTOMIZE FONTS IN YOUR PLOTS
#
#################################### LOAD LIBRARIES ############################

library(data.table)
library(ggplot2)
library(zoo)
library(janitor)
library(scales)
library(tidyverse)
options(scipen = 999)

#################################### LOAD DATA #################################

subarea_fatalities <- fread("data/episode2/fatalities_fatality_rate_2020.csv")


base_theme <- theme(
    # THIS CHANGES ALL THTE FONT IN THE PLOT EXCEPT FOR THE LABELS. THAT 
    # NEEDS TO BE SPECIFIED IN THE GEOM_LABEL OR GEOM_TEXT() FUNCTIONS
    text = element_text(family = "Roboto"),
    axis.text = element_text(size = 12),
    plot.title = element_text(hjust = 0.5, size = 16, face = "bold"),
    plot.subtitle = element_text(hjust = 0.5, size = 12, face = "italic",
                                 family = "Roboto Thin"),
    legend.position = "bottom",
    legend.title = element_blank(),
    plot.background = element_blank(),
    panel.background = element_blank(),
    axis.line = element_line(color = "black", size = 2),
    legend.box.background = element_blank(),
    legend.background = element_blank()
  )

theme_set(base_theme)

# OR... INSTEAD OF SETTING A GLOBAL THEME, YOU CAN CREATE MANY THEMES AND THEN 
# ADD THEM TO YOUR CHARTS
# EG 
# ggplot() + 
  # geom_line() + 
  # base_theme

#################################### SCRIPT ####################################

######### EPISODE 3 CONTENT ----------------------------------------------------

library(extrafont)

# IMPORT THE FONTS YOU HAVE LOCALLY ALREADY
extrafont::font_import(prompt = FALSE)

# LIST OUT THE FONTS AVAILABLE TO YOU
extrafont::fonts()

# WHAT TO DO WHEN YOU NEED A NEW FONT? 
# https://support.microsoft.com/en-us/topic/download-and-install-custom-fonts-to-use-with-office-0ee09e74-edc1-480c-81c2-5cf9537c70ce 
# 
# DOWNLOAD & INSTALL ROBOTO FONT
# https://fonts.google.com/specimen/Roboto#standard-styles 

# DOWNLOAD AGAIN
extrafont::font_import(prompt = FALSE)

# YOU CAN NOW SET 'FAMILY' IN YOUR THEME USING ANYONE OF THE fonts()

# CLEAN DATA
subarea_fatalities_c <- subarea_fatalities %>% 
  # THIS PUTS YOUR COLUMN NAMES INTO R's STANDARD NAMING CONVENTION
  janitor::clean_names() %>% 
  # REMOVE ROWS WHICH ARE ALL EMPTY. CAN SPECIFY ROWS VS COLUMNS
  janitor::remove_empty(quiet = TRUE) %>% 
  filter(!is.na(year)) %>% 
  # FROM THE READR PACKAGE, THIS FUNCTION REALLY EASILY TAKES CARE OF
  # NUMBER FORMATTING ISSUES WHEN TRANSFORMING FROM CHARACTER VALUES. 
  mutate(total_fatalities = readr::parse_number(total_fatalities),
         month_num = match(month, month.name),
         date = as.yearmon(paste(year, month_num), format = "%Y %m"))

# AREA FATALITIES
# PIVOT DATA INTO LONG FORMAT
area_fatalities <- subarea_fatalities_c %>% 
  pivot_longer(
    cols = c(
      rural_interstate,
      urban_interstate,
      rural_arterial,
      urban_arterial,
      rural_collector_local,
      urban_collector_local
    ),
    names_to = "roadway_type",
    values_to = "fatality_pct"
  ) %>% 
  select(date, total_fatalities, roadway_type, fatality_pct) %>% 
  mutate(fatality_pct = parse_number(fatality_pct) / 100,
         roadway_type = gsub("_", " ", roadway_type),
         roadway_clean = str_to_title(roadway_type),
         color = ifelse(roadway_type == "rural collector local", "#406583", "gray")
  )

# LINE CHART
ggplot(area_fatalities) +
  geom_line(mapping = aes(x = date, y = fatality_pct, color = roadway_type, 
                          group = roadway_type)) + 
  scale_y_continuous(labels = percent) + 
  labs(y = "Percent of Total Fatalities")

ggplot(area_fatalities) +
  # LAYERS 
  # NOTE - LAYERS ARE ADDING FROM THE BOTTOM TO THE TOP. THE FIRST LAYER ADDED 
  # WILL BE AT THE BOTTOM OF YOUR CHART. BUILDING AS YOU GO. 
  geom_rect(mapping = aes(xmin = as.yearmon("Jan 2020"), 
                          xmax = as.yearmon("May 2020"),
                          ymin = 0, ymax = max(fatality_pct, na.rm = TRUE)),
            fill = "gray90") + 
  geom_line(mapping = aes(x = date, y = fatality_pct,
                          color = color, 
                          group = roadway_clean)) + 
  geom_point(mapping = aes(x = date, y = fatality_pct,
                           color = color,
                           shape = roadway_clean, 
                           group = roadway_clean),
             size = 2) + 
  geom_label(data = filter(area_fatalities, roadway_type == "rural collector local",
                           date %in% c(as.yearmon("Jan 2020"), as.yearmon("Apr 2020"))),
             mapping = aes(x = date, y = fatality_pct, 
                           label = paste0(fatality_pct * 100, "%")),
             fill = rgb(1, 1, 1, .5), vjust = 0, nudge_y = .005) + 
  # SETTINGS
  # LABELS = PART OF THE SCALES LIBRARY
  scale_y_continuous(labels = percent, 
                     expand = c(0, 0),
                     limits = c(0, max(area_fatalities$fatality_pct * 1.05))
  ) + 
  # TELLS GGPLOT 
  scale_color_identity() + 
  labs(y = "Percent of Total Fatalities", 
       title = "Percent of Total Roadway Fatalities by Roadway Facility",
       subtitle = "Fatalities Rose By 5% On Rural Collectors Between January and April 2020",
       x = "")

# AREA FATALITIES
# PIVOT DATA INTO LONG FORMAT
gender_fatalities <- subarea_fatalities_c %>% 
  pivot_longer(
    cols = c(
      male,
      female
    ),
    names_to = "gender",
    values_to = "fatality_pct"
  ) %>% 
  select(date, total_fatalities, gender, fatality_pct) %>% 
  mutate(fatality_pct = parse_number(fatality_pct) / 100,
         gender = gsub("_", " ", gender),
         gender_clean = str_to_title(gender),
         color = ifelse(gender == "female", "#406583", "gray")
  )


ggplot(gender_fatalities) +
  # LAYERS 
  # NOTE - LAYERS ARE ADDING FROM THE BOTTOM TO THE TOP. THE FIRST LAYER ADDED 
  # WILL BE AT THE BOTTOM OF YOUR CHART. BUILDING AS YOU GO. 
  geom_rect(mapping = aes(xmin = as.yearmon("Jan 2020"), 
                          xmax = as.yearmon("May 2020"),
                          ymin = 0, ymax = max(fatality_pct, na.rm = TRUE)),
            fill = "gray90") + 
  geom_line(mapping = aes(x = date, y = fatality_pct,
                          color = color, 
                          group = gender_clean)) + 
  geom_point(mapping = aes(x = date, y = fatality_pct,
                           color = color,
                           shape = gender_clean, 
                           group = gender_clean),
             size = 2) + 
  # SETTINGS
  # LABELS = PART OF THE SCALES LIBRARY
  scale_y_continuous(labels = percent, 
                     expand = c(0, 0),
                     limits = c(0, max(gender_fatalities$fatality_pct * 1.05))
  ) + 
  # TELLS GGPLOT 
  scale_color_identity() + 
  labs(y = "Percent of Total Fatalities", 
       title = "Percent of Total Roadway Fatalities by Roadway Facility",
       x = "")
