#################################### INFO ######################################
#
# AUTHOR: Rob Owens
# DATE: 4/11/22
# PURPOSE: EPISODE 4 — geom_smooth(), geomtextpath, and ggplot extensions
#
#################################### LOAD LIBRARIES ############################

library(data.table)
library(dplyr)
library(ggplot2)
library(geomtextpath)
library(tidyverse)
library(scales)
options(scipen = 999)

#################################### LOAD DATA #################################

# load package and data
data(mpg, package = "ggplot2")
subarea_fatalities <- fread("data/episode2/fatalities_fatality_rate_2020.csv")

#################################### SCRIPT ####################################

mpg_select <- mpg[mpg$manufacturer %in% c("audi", "ford", "honda", "hyundai"), ]

# Scatterplot
theme_set(theme_bw())  # pre-set the bw theme.
g <- ggplot(mpg_select, aes(displ, cty)) + 
  labs(subtitle = "mpg: Displacement vs City Mileage",
       title = "Bubble chart")

# GEOM SMOOTH CREATES AUTOMATIC TREND LINES. DEFAULTS TO LOESS 
# REGRESSION
g + geom_jitter(aes(col = manufacturer, size=hwy), alpha = .5) + 
    geom_smooth(aes(col = manufacturer))

# REMOVING STANDARD ERROR
g + geom_jitter(aes(col = manufacturer, size=hwy), alpha = .5) + 
  geom_smooth(aes(col = manufacturer), se = FALSE)

# CHANGING THE TYPE OF TREND LINE
g + geom_jitter(aes(col = manufacturer, size = hwy), alpha = .5) + 
  geom_smooth(aes(col = manufacturer), se = FALSE, method = "lm")

# SPECIFY YOUR OWN FORMULA
g + geom_jitter(aes(col = manufacturer, size = hwy), alpha = .5) +
  geom_smooth(aes(col = manufacturer), method = "lm", se = F,
              formula = y ~ poly(x, 2))

model <- lm(cty ~ displ, data = mpg_select)
summary(model)

yintercept <- model$coefficients[1]
slope <- model$coefficients[2]
formula <- paste0("cty = ", slope, "disp + ", yintercept)


# GEOMTEXT PATH

# CLEAN DATA
subarea_fatalities_c <- subarea_fatalities %>% 
  # THIS PUTS YOUR COLUMN NAMES INTO R's STANDARD NAMING CONVENTION
  janitor::clean_names() %>% 
  # REMOVE ROWS WHICH ARE ALL EMPTY. CAN SPECIFY ROWS VS COLUMNS
  janitor::remove_empty(quiet = TRUE) %>% 
  filter(!is.na(year)) %>% 
  # FROM THE READR PACKAGE, THIS FUNCTION REALLY EASILY TAKES CARE OF
  # NUMBER FORMATTING ISSUES WHEN TRANSFORMING FROM CHARACTER VALUES. 
  mutate(total_fatalities = readr::parse_number(total_fatalities),
         month_num = match(month, month.name),
         date = as.yearmon(paste(year, month_num), format = "%Y %m"))

# AREA FATALITIES
# PIVOT DATA INTO LONG FORMAT
area_fatalities <- subarea_fatalities_c %>% 
  pivot_longer(
    cols = c(
      rural_interstate,
      urban_interstate,
      rural_arterial,
      urban_arterial,
      rural_collector_local,
      urban_collector_local
    ),
    names_to = "roadway_type",
    values_to = "fatality_pct"
  ) %>% 
  select(date, total_fatalities, roadway_type, fatality_pct) %>% 
  mutate(fatality_pct = parse_number(fatality_pct) / 100,
         roadway_type = gsub("_", " ", roadway_type),
         roadway_clean = str_to_title(roadway_type),
         color = ifelse(roadway_type == "rural collector local", "#406583", "gray")
  )

# LINE CHART
ggplot(area_fatalities) +
  geom_line(mapping = aes(x = date, y = fatality_pct, color = roadway_type, 
                          group = roadway_type)) + 
  scale_y_continuous(labels = percent) + 
  labs(y = "Percent of Total Fatalities")

ggplot(area_fatalities) +
  # LAYERS 
  # NOTE - LAYERS ARE ADDING FROM THE BOTTOM TO THE TOP. THE FIRST LAYER ADDED 
  # WILL BE AT THE BOTTOM OF YOUR CHART. BUILDING AS YOU GO. 
  geom_rect(mapping = aes(xmin = as.yearmon("Jan 2020"), 
                          xmax = as.yearmon("May 2020"),
                          ymin = 0, ymax = max(fatality_pct, na.rm = TRUE)),
            fill = "gray90") + 
  geom_textpath(aes(x = date, y = .2, label = "Target 20%"),
                color = "red", alpha = .5) + 
  geom_line(mapping = aes(x = date, y = fatality_pct,
                          color = color, 
                          group = roadway_clean)) + 
  geom_point(mapping = aes(x = date, y = fatality_pct,
                           color = color,
                           shape = roadway_clean, 
                           group = roadway_clean),
             size = 2) + 
  geom_label(data = filter(area_fatalities, roadway_type == "rural collector local",
                           date %in% c(as.yearmon("Jan 2020"), as.yearmon("Apr 2020"))),
             mapping = aes(x = date, y = fatality_pct, 
                           label = paste0(fatality_pct * 100, "%")),
             fill = rgb(1, 1, 1, .5), vjust = 0, nudge_y = .005) + 
  # SETTINGS
  # LABELS = PART OF THE SCALES LIBRARY
  scale_y_continuous(labels = percent, 
                     expand = c(0, 0),
                     limits = c(0, max(area_fatalities$fatality_pct * 1.05))
  ) + 
  # TELLS GGPLOT 
  scale_color_identity() + 
  labs(y = "Percent of Total Fatalities", 
       title = "Percent of Total Roadway Fatalities by Roadway Facility",
       subtitle = "Fatalities Rose By 5% On Rural Collectors Between January and April 2020",
       x = "")


ggplot(area_fatalities) +
  # LAYERS 
  # NOTE - LAYERS ARE ADDING FROM THE BOTTOM TO THE TOP. THE FIRST LAYER ADDED 
  # WILL BE AT THE BOTTOM OF YOUR CHART. BUILDING AS YOU GO. 
  geom_rect(mapping = aes(xmin = as.yearmon("Jan 2020"), 
                          xmax = as.yearmon("May 2020"),
                          ymin = 0, ymax = max(fatality_pct, na.rm = TRUE)),
            fill = "gray90") + 
  geom_textline(mapping = aes(x = date, y = fatality_pct,
                              label = roadway_clean,
                              color = color, 
                              group = roadway_clean), 
                spacing = 100,
                show.legend = FALSE) + 
  geom_label(data = filter(area_fatalities, roadway_type == "rural collector local",
                           date %in% c(as.yearmon("Jan 2020"), as.yearmon("Apr 2020"))),
             mapping = aes(x = date, y = fatality_pct, 
                           label = paste0(fatality_pct * 100, "%")),
             fill = rgb(1, 1, 1, .5), vjust = 0, nudge_y = .005) + 
  # SETTINGS
  # LABELS = PART OF THE SCALES LIBRARY
  scale_y_continuous(labels = percent, 
                     expand = c(0, 0),
                     limits = c(0, max(area_fatalities$fatality_pct * 1.05))
  ) + 
  # TELLS GGPLOT 
  scale_color_identity() + 
  labs(y = "Percent of Total Fatalities", 
       title = "Percent of Total Roadway Fatalities by Roadway Facility",
       subtitle = "Fatalities Rose By 5% On Rural Collectors Between January and April 2020",
       x = "")
